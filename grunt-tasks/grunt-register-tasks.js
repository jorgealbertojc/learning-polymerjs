


( () => {



    module.exports = ( _grunt ) => {



        _grunt.registerTask( 'default', () => {
            console.log('')
            console.log('')
            console.log( '          wTF, que quieres hacer con grunt en default !!!'.bold.red )
            console.log('')
        } )



        _grunt.registerTask( 'build', [
            'clean',
            'copy',
            'stylus',
            'postcss',
            'babel',
            'pug',
            'connect:server',
            'watch',
        ] )



        _grunt.registerTask( 'dev', 'ok to build app', ( _target = null ) => {



            if ( ! _target ) {
                _grunt.task.run( [ 'build' ] )
                return null
            }



            _grunt.task.run( [
                'clean',
                'copy:components',
                'copy:' + _target + '-images',
                'stylus:' + _target + '-styles',
                'stylus:' + _target + '-styles-components',
                'postcss:' + _target + '-styles',
                'postcss:' + _target + '-styles-components',
                'babel:' + _target + '-scripts',
                'babel:' + _target + '-scripts-components',
                'pug:' + _target + '-views',
                'pug:' + _target + '-scripts-components',
                'pug:' + _target + '-index',
                'pug:' + _target + '-webcomponents',
                'polybuild:' + _target + '-vulcanize',
                'connect:' + _target + '-server',
                'watch',
            ] )
        } )




        return null
    }
} )()
