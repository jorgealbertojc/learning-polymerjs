


( () => {



    module.exports = ( _grunt ) => {



        var fs = require( 'fs' )
        var cheerio = require( 'cheerio' )



        const get_components_list_from_components_dir = ( _dir_components ) => {
            if ( fs.existsSync( _dir_components ) ) {
                return fs.readdirSync( _dir_components )
            }
            return []
        }



        const get_file_object_content = ( _component_html_filepath ) => {
            var file_contents
            if ( fs.existsSync( _component_html_filepath ) ) {
                file_contents = _grunt.file.read( _component_html_filepath )
                return cheerio.load( file_contents )
            }
            return cherio.load( '' )
        }



        const get_source_attr_value_for_context = ( _$, _element = 'style' ) => {
            var elements
            var sources = []
            elements = _$( 'dom-module' ).find( _element )
            for ( let i = 0 ; i < elements.length ; i++ ) {
                if ( elements[ i ].attribs && elements[ i ].attribs.src ) {
                    sources.push( _grunt.appconf.dirs.dev + elements[ i ].attribs.src )
                }
            }
            return sources
        }



        const get_contents_from_source_paths = ( _array_of_files = [] ) => {
            var file_contents = ''
            _array_of_files.forEach( ( _src ) => {
                file_contents += _grunt.file.read( _src )
            } )
            return file_contents
        }



        const build_updated_file_contents = ( _$, _styles_contents = '', _scripts_contents = '' ) => {
            if ( _styles_contents.length ) {
                _$( 'dom-module' ).find( 'style' ).remove()
                _$( 'dom-module' ).prepend( '\n<style>\n' + _styles_contents + '\n</style>\n' )
            }
            if ( _scripts_contents.length ) {
                _$( 'dom-module' ).find( 'script' ).remove()
                _$( 'dom-module' ).append( '\n<script>\n' + _scripts_contents + '\n</script>\n' )
            }
            return _$.html()
        }



        const update_file_contents = ( _filepath, _filecontents = '' ) => {
            _grunt.file.write( _filepath, _filecontents )
        }



        const update_files_with_styles_and_scripts = ( _dir_components = './', _list_components = [] ) => {
            var styles_paths = []
            var scripts_paths = []
            var $
            var updated_files = 0
            if ( _list_components && _list_components.length ) {
                _list_components.forEach( ( _component_filename ) => {
                    var styles_contents
                    var scripts_contents
                    var updated_file_contents
                    var filepath = _dir_components + '/' + _component_filename + '/' + _component_filename + '.html'
                    if ( filepath && fs.existsSync( filepath ) ) {
                        $ = get_file_object_content( filepath )
                        styles_paths = get_source_attr_value_for_context( $, 'style' )
                        scripts_paths = get_source_attr_value_for_context( $, 'script' )
                        styles_contents = get_contents_from_source_paths( styles_paths )
                        scripts_contents = get_contents_from_source_paths( scripts_paths )
                        updated_file_contents = build_updated_file_contents( $, styles_contents, scripts_contents )
                        update_file_contents( filepath, updated_file_contents )
                        updated_files = updated_files + 1
                    }
                } )
            }
            return updated_files
        }




        _grunt.appconf.apps.forEach( ( _app ) => {

            var app_component_dir = _grunt.appconf.dirs.dev + '/' + _app + '/scripts/components'

            _grunt.registerTask( 'polybuild:' + _app + '-vulcanize', () => {

                var components_list = get_components_list_from_components_dir( app_component_dir )
                var updated_files = 0

                if ( components_list && components_list.length ) {
                    updated_files = update_files_with_styles_and_scripts( app_component_dir, components_list )
                }
                console.log( '>>'.green, updated_files + ' file' + ( ( updated_files === 1 ) ? '' : 's' ) + ' poly-vulcanized' )
            } )
        } )
    }
} )()
