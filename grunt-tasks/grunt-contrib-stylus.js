


( () => {



    module.exports = ( _grunt ) => {



        var api = {
            name: 'stylus',
            config: {}
        }



        _grunt.appconf.apps.forEach( ( _app ) => {



            //- styles
            api.config[ _app + '-styles' ] = {
                files: [ {
                    expand: true,
                    cwd: _grunt.appconf.dirs.apps + '/' + _app + '/styles',
                    src: [
                        '**/*.styl',
                        '!**/_*.styl'
                    ],
                    dest: _grunt.appconf.dirs.dev + '/' + _app + '/styles',
                    ext: '.css'
                } ]
            }



            //- styles-components
            api.config[ _app + '-styles-components' ] = {
                options: {
                    compress: false
                },
                files: [ {
                    expand: true,
                    cwd: _grunt.appconf.dirs.apps + '/' + _app + '/scripts/components',
                    src: [
                        '**/*.styl',
                        '!**/_*.styl'
                    ],
                    dest: _grunt.appconf.dirs.dev + '/' + _app + '/scripts/components',
                    ext: '.css'
                } ]
            }
        } )



        return api
    }
} )()
