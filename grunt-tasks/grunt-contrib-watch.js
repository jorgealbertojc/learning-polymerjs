


( () => {



    module.exports = ( _grunt ) => {



        var api = {
            name: 'watch',
            config: {}
        }



        api.config.options = {
            livereload: true
        }



        _grunt.appconf.apps.forEach( ( _app ) => {



            // scripts
            api.config[ _app + '-scripts' ] = {
                files: [ _grunt.appconf.dirs.apps + '/' + _app + '/scripts/**/*.js' ],
                tasks: [ 'babel:' + _app + '-scripts' ]
            }



            // scripts-components
            api.config[ _app + '-scripts-components' ] = {
                files: [ _grunt.appconf.dirs.apps + '/' + _app + '/scripts/components/**/*.js' ],
                tasks: [ 'babel:' + _app + '-scripts-components', 'polybuild:' + _app + '-vulcanize' ]
            }



            // styles
            api.config[ _app + '-styles' ] = {
                files: [ _grunt.appconf.dirs.apps + '/' + _app + '/styles/**/*.styl' ],
                tasks: [ 'stylus:' + _app + '-styles', 'postcss:' + _app + '-styles' ]
            }



            // styles-components
            api.config[ _app + '-styles-components' ] = {
                files: [ _grunt.appconf.dirs.apps + '/' + _app + '/scripts/components/**/*.styl' ],
                tasks: [ 'stylus:' + _app + '-styles-components', 'postcss:' + _app + '-styles-components', 'polybuild:' + _app + '-vulcanize' ]
            }



            // views
            api.config[ _app + '-views' ] = {
                files: [ _grunt.appconf.dirs.apps + '/' + _app + '/views/**/*.pug' ],
                tasks: [ 'pug:' + _app + '-views' ]
            }



            // webcomponents
            api.config[ _app + '-webcomponents' ] = {
                files: [ _grunt.appconf.dirs.apps + '/' + _app + '/webcomponents/**/*.pug' ],
                tasks: [ 'pug:' + _app + '-webcomponents' ]
            }



            // webcomponents
            api.config[ _app + '-scripts-components' ] = {
                files: [ _grunt.appconf.dirs.apps + '/' + _app + '/scripts/components/**/*.pug' ],
                tasks: [ 'pug:' + _app + '-scripts-components', 'polybuild:' + _app + '-vulcanize' ]
            }



            // index
            api.config[ _app + '-index' ] = {
                files: [ _grunt.appconf.dirs.apps + '/' + _app + '/index.pug' ],
                tasks: [ 'pug:' + _app + '-index' ]
            }
        } )



        return api
    }
} )()
