# learning-polymerjs

Desarrollo de aplicaciones de ejemplo para poder aprender polymerjs esta
iniciativa se tomo como solución debido a que bancomer impuso como estandar
para todos sus desarrollos web a PolymerJS.

### registro de nueva aplicación

Para registrar una nueva aplicación y que funcione correctamente con las
tareas de grunt esta se debe registrar el la parte de `apps` en el archivo
de configuración `apps-config.json`.

Una vez registrada la nueva app se debe crear el directorio con el mismo
nombre en el directorio `src` con la siguiente estructura:

```bash
your-app-name
├── images
├── index.pug
├── scripts
├── styles
└── views
```


### arranque del proyecto

Sólo debes ejecutar los siguientes comandos:

**Instalación**

```bash
npm install
```

```bash
bower install
```

**Desarrollo**

```bash
grunt dev
```

Para desplegar una aplicación en específico

```bash
grunt dev:your-app-name
```
> _ el nombre de la aplicación es el mismo nombre con el que se registro en el archivo `apps-config.json` _

**p.e.**

```bash
grunt dev:polymer-starter
```
